public class DecimalCounter {
    private int value;
    private final int highLimit;
    private final int lowLimit;

    public DecimalCounter(int highLimit, int lowLimit) {
        this.value = lowLimit;
        this.highLimit = highLimit;
        this.lowLimit = lowLimit;
    }
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getHighLimit() {
        return highLimit;
    }

    public int getLowLimit() {
        return lowLimit;
    }

    public void increasing(){
        if (this.getValue() < this.getHighLimit()){
            this.setValue(this.getValue() + 1);
        }else {
            this.setValue(this.getLowLimit());
        }
    }
    public void decreasing(){
        if (this.getValue() > this.getLowLimit()){
            this.setValue(this.getValue() - 1);
        }else {
            this.setValue(this.getHighLimit());
        }
    }
    public void displayTheValue(){
        System.out.println(this.getValue());
    }
}
