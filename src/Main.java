import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter the limits: ");
        Scanner scanner = new Scanner(System.in);
        int lowLimit = scanner.nextInt();
        int highLimit = scanner.nextInt();
        DecimalCounter a = new DecimalCounter(highLimit, lowLimit);
        a.displayTheValue();
        a.increasing();
        a.displayTheValue();
        a.increasing();
        a.displayTheValue();
        a.increasing();
        a.displayTheValue();
        a.decreasing();
        a.displayTheValue();
        a.decreasing();
        a.decreasing();
        a.decreasing();
        a.displayTheValue();
    }
}